/**
 * Created by nobodywithbody on 03.03.16.
 */
"use strict";
const co = require('co');
const request = require('co-request');
const colors = require('colors');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');
const config = require('config.json')('./config.json');
const wait = require('co-flow').wait;
const cheerio = require('cheerio');
const _ = require('underscore');

colors.setTheme({
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red'
});

const USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36';

class Worker {
    constructor() {
        this.model = mongoose.model(config.name, config.model);

        process.on('message', (data) => {
            this.stage = data.stage;
            this.letsRock.apply(this, [data.work, data.proxy]);
        });

        process.send({type: 'need work', pid: process.pid});
    }

    returnErrorData(data, pid, proxy) {
        process.send({type: 'parse error', row: data, pid: pid, proxy: proxy});
    }

    parse(body, reg) {
        if (reg.selector) {
            let $ = cheerio.load(body);
            if (reg.value) {
                return $(reg.selector)[reg.function](reg.value);
            } else {
                return $(reg.selector)[reg.function]();
            }
        }
        let links = body.match(reg);
        return [...new Set(links)];
    }

    letsRock(work, proxy) {
        var self = this;
        var model;
        co(function*() {
            if (config.request_delay) {
                yield wait(config.request_delay);
            }
            let stage = config.task.stages[self.stage];

            if (!proxy) {
                self.returnErrorData(work, process.pid, proxy);
                return;
            }

            var response = yield self.makeRequest(work.link, proxy);
            if (self.hasError(response)) {
                self.returnErrorData(work, process.pid, proxy);
                return false;
            }
            let $ = cheerio.load(response.body);
            let refresh = $('meta[http-equiv="refresh"]');
            if (refresh.length > 0) {
                var url_refresh = refresh.attr('content').match(/url=(.+)/)[1];
                response = yield self.makeRequest(url_refresh, proxy);
            }
            if (stage.type == 'collect_link') {
                let reg = new RegExp(stage['link'], 'g');
                let links = self.parse(response.body, reg);
                console.log(colors.info('SUCCESS LOAD. COUNT:' + links.length));
                if (links.length == 0) {
                    console.log(colors.warn('EMTY LINKS:' + work.url));
                }
                for (let j in links) {
                    model = new self.model({'link': links[j], 'process': false});
                    model.processed = false;
                    yield model.save();
                }
                process.send({type: 'end work', pid: process.pid, proxy: proxy});
            } else if (stage.type == 'collect_data') {
                model = yield self.model.findById(work['_id']);
                console.log(colors.info('SUCCESS LOAD:' + model.id));

                for (let j in stage.parse) {
                    let parse_value = self.parse(response.body, stage.parse[j]);
                    model[j] = parse_value ? parse_value : null;
                }
                model.processed = true;
                yield model.save();
                process.send({type: 'end work', pid: process.pid, proxy: proxy});
            }

        }).catch(function (err) {
            console.log('worker:letsRock', err);
        });
    }

    makeRequest(url, proxy) {
        return co(function*() {
            let errorReturn = false;

            return yield request.get({
                url: url,
                headers: {
                    'User-Agent': USER_AGENT,
                    'Content-Type': "text/plain; charset=utf-8",
                    //"Accept-Encoding": 'gzip, deflate, sdch',
                    "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
                    "Accept": " */*"
                },
                proxy: config.proxy ? proxy.host : '',
                timeout: 5000
            }).catch(function (e) {
                console.log(colors.error('EXCEPTION REQUEST:' + e));
                errorReturn = true;
            });
        })
    }

    hasError(response) {
        if (!response) {
            console.log(colors.error('EMPTY RESPONSE:' + response));
            return true;
        }
        if (response.statusCode !== 200) {
            console.log(colors.error('STATUS CODE IS NOT 200: ' + response.statusCode));
            return true;
        }

        return false;
    }
}

module.exports = function (db) {
    return new Worker(db);
};

