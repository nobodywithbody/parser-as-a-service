"use strict";
const co = require('co');
const cluster = require('cluster');
const Server = require('ws').Server;
const EventEmitter = require('events');
const config = require('config.json')('./config.json');
class ConfigEmitter extends EventEmitter {
}
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

class Master {
    constructor() {
        let self = this;
        this.emitter = new ConfigEmitter();
        this.emitter.on('proxy:load:complete', function () {
            if (self.workers.length == 0) {
                self.createWorkers();
            }
        });
        this.emitter.on('task_manager:stage:complete', function () {
            console.log('stage complete');
            for (let i in self.workers) {
                self.sendWork(self.workers[i]);
            }
        });

        this.countWorkers = config.count_workers;

        this.workers = [];
        this.model = mongoose.model(config.name, config.model);
        this.taskManager = require('./task-manager')(this.model, this.emitter);
        this.proxyHandler = require('./proxy-handler')(this.emitter);
        if (config.proxy_source.file) {
            this.proxyHandler.setFile(config.proxy_source.file)
        } else {
            this.proxyHandler.loadProxy();
        }
        //this.wss = new Server({port: 8080});
        //this.wss.on('connection', (ws) => {
        //    ws.on('close', (client) => {
        //        for (let i = 0; i < this.clients.length; i++) {
        //            if (client == this.clients[i]) {
        //                this.clients.splice(i, 1);
        //            }
        //        }
        //    });
        //    this.clients.push(ws);
        //});

        cluster.on('message', (response) => {
            var worker = null;
            if (response.pid) {
                for (let i = 0; i < this.workers.length; i++) {
                    if (response.pid == this.workers[i].process.pid) {
                        worker = this.workers[i];
                        break;
                    }
                }
            }

            switch (response['type']) {
                case 'parse error':
                    this.taskManager.addErrorTask(response.row);
                    this.proxyHandler.errorRequest(response.proxy['id']);
                    break;
                case 'end work':
                    this.proxyHandler.successRequest(response.proxy['id']);
                    break;
                case 'need work':
            }

            this.sendWork(worker);
        });

        cluster.on('exit', (worker, code, signal) => {
            console.log(`worker ${worker.process.pid} died`);
        });
    }

    createWorkers() {
        var self = this;
        co(function*() {
            for (var i = 0; i < self.countWorkers; i++) {
                let worker = cluster.fork();
                self.workers.push(worker);
            }

        }).catch(function (e) {
            console.log(e)
        });
    }

    sendWork(worker) {
        var self = this;
        co(function*() {
            let task = yield self.taskManager.getWork();
            if (!task) {
                return false;
            }
            worker.send({
                work: task,
                proxy: self.proxyHandler.getProxy(),
                stage: self.taskManager.current_stage
            });
        }).catch(function (e) {
            console.log('master:send_work', e);
        });
    };
}

module.exports = function (params) {
    return new Master(params);
};