/**
 * Created by nobodywithbody on 13.03.16.
 */
"use strict";
const ucfirst = require('ucfirst');
const co = require('co');
const _ = require('underscore');
const config = require('config.json')('./config.json');

class TaskManager {
    constructor(model, emitter) {
        this.stages = config.task.stages;
        if (this.stages.length == 0) {
            throw new Error('Empty stages error');
        }
        this.actions = [];
        this.model = model;

        for (let i in this.stages) {
            let type = this.stages[i].type;
            this.actions[type] = require('./actions/' + type);
            let valid = this.actions[type].validate(this.stages[i]);
            if (!valid) {
                throw new Error(`Action ${type} not validate input data`);
            }
        }
        this.countEndWork = 0;
        this.emitter = emitter;
        this.changeStage();
    }

    getWork() {
        let self = this;
        return co(function* () {
            if (self.isEnd()) {
                return new Promise((resolve, reject) => {
                    resolve(false);
                })
            }
            let stage = self.stages[self.current_stage];
            let action = self.actions[stage.type];
            if (yield action.isEmpty()) {
                self.toggleEndWork();
                return new Promise((resolve, reject) => {
                    resolve(false);
                });
            }
            let task = action.getWork();
            return new Promise((resolve, reject) => {
                resolve(task);
            })
        });
    }

    isEnd() {
        if (!this.stages[this.current_stage]) {
            return true;
        }

        return false;
    }

    addErrorTask(work) {
        let type = this.stages[this.current_stage].type;
        let action = this.actions[type];
        action.addErrorTask(work);
    }

    toggleEndWork() {
        var self = this;
        co(function*() {
            self.countEndWork++;
            if (self.countEndWork == config.count_workers) {
                self.countEndWork = 0;
                yield self.changeStage();
                self.emitter.emit('task_manager:stage:complete');
            }
        })

    }

    changeStage() {
        var self = this;
        return co(function* () {
            if (_.isUndefined(self.current_stage)) {
                self.current_stage = 0;
            } else {
                self.current_stage++;
            }

            if (self.current_stage >= self.stages.length) {
                return false;
            }

            let type = self.stages[self.current_stage].type;
            let action = self.actions[type];
            action.init(self.stages[self.current_stage], self.model);
        })

    }

    countTotalTask() {
        return this.config.page_number_count;
    }
}

module.exports = function (model, emitter) {
    return new TaskManager(model, emitter);
};