/**
 * Created by nobodywithbody on 03.04.16.
 *
 *  {
                "type": "collect_data",
                "link_attribute": "link",
                "parse": {
                    "salary": {
                        "selector" : ".l-paddings [itemprop=\"baseSalary\"]",
                        "function": "attr",
                        "value": "content"
                    }
                }
            }
 */
"use strict";

const co = require('co');
const lock = require('co-lock');

module.exports = {
    tasks: [],
    errorTasks: [],
    init: function(stage, model) {
        this.stage = stage;
        this.model = model;
    },
    validate: function () {
        return true;
    },
    generator: function* (stage, tasks) {
        for (let i = 0; i < tasks.length; i++) {
            yield tasks.shift();
        }
    },
    isEmpty: function() {
        var self = this;
        return co(function*() {
            if (self.errorTasks.length > 0) {
                self.val = self.errorTasks.shift();
                return false;
            }

            var release = yield lock(1);

            if (self.tasks.length == 0) {
                yield self.loadFromDb();
                self.genObject = new self.generator(self.stage, self.tasks);
            }

            yield release;
            if (self.tasks.length > 0) {
                self.val = self.tasks.shift();
                return false;
            }

            return true;
        })
    },
    addErrorTask: function(work) {
        this.errorTasks.push(work);
    },
    getWork: function() {
        return this.val;
    },
    loadFromDb: function() {
        let self = this;
        return co(function* () {
            self.tasks = yield self.model.find({processed: false}).limit(10).exec();
        })
    }
};