/**
 * Created by nobodywithbody on 03.04.16.{
    "type": "collect_link",
    "base": "https://hh.ru/search/vacancy",
    "params": {
    "clusters": "true",
        "area": "1",
        "enable_snippets": "true",
        "text": "разработчик"
},
    "page_attribute": "page",
    "start_number": "0",
    "end_number": "2",
    "link": "(https:\\/\\/hh\\.ru\\/vacancy\\/\\d+)"
    },
 *
 */
"use strict";
const querystring = require('querystring');
const co = require('co');

module.exports = {
    errorTasks: [],
    init: function (stage) {
        this.stage = stage;
    },
    validate: function (data) {
        return true;
    },
    generator: function*(stage) {
        for (let i = stage.start_number; i <= stage.end_number; i++) {

            let params = querystring.stringify(stage.params);
            yield {
                link: stage.base + '?' + stage.page_attribute + '=' + i + '&' + params,
                parse: stage.link_regexp
            }

        }
    },
    getWork: function () {
        if (this.val.value) {
            this.val.value.stage = this.stage;
        }

        return this.val.value;
    },
    isEmpty: function () {
        let self = this;
        return co(function*() {
            if (self.errorTasks.length > 0) {
                self.val = {value: self.errorTasks.shift()}
                return false;
            }

            if (!self.genObject) {
                self.genObject = new self.generator(self.stage)
            }

            self.val = self.genObject.next();
            if (self.val.value && !self.val.done) {
                return false;
            }


            return true;
        })

    },
    addErrorTask: function (work) {
        this.errorTasks.push(work);
    }
};