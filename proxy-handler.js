/**
 * Created by nobodywithbody on 15.02.16.
 */
"use strict";
const random = require('random-js')();
const co = require('co');
const request = require('co-request');
const csv = require('csv');

class ProxyHandler {
    constructor(emitter) {
        this.proxyList = [];
        this.emitter = emitter;
    }
    setFile(file_name) {
        let proxies = require(`./${file_name}`);
        for (let i = 0; i < proxies.length; i++) {
            this.proxyList.push({
                host: 'http://' + proxies[i],
                errorRequest: 0,
                totalRequest: 0
            });
        }
        this.emitter.emit('proxy:load:complete');
    }

    loadProxy() {
        let self = this;
        co(function*() {
            var response = yield request.get({
                url: 'http://hideme.ru/api/proxylist.php?out=csv&maxtime=2000&anon=34&type=hs',
            }).catch(function (e) {
                console.log(e);
            });

            csv.parse(response.body, {delimiter: ';'}, function (err, data) {
                let proxies = [];
                for (let i = 0; i < data.length; i++) {
                    proxies.push({
                        host: 'http://' + data[i][1] + ':' + data[i][2],
                        errorRequest: 0,
                        totalRequest: 0
                    });
                    self.proxyList = proxies;
                }

                self.emitter.emit('proxy:load:complete');
                setInterval(function () {
                    self.loadProxy();
                }, 1000 * 60 * 60 * 4)
            });
        }).catch(function (e) {
            console.log('Proxy load', e);
        });
    }

    getProxyLength() {
        return this.proxyList.length;
    }

    getProxy() {
        let numProxy = random.integer(0, this.proxyList.length - 1);

        return {
            id: numProxy,
            host: this.proxyList[numProxy].host
        }
    }

    successRequest(id) {
        if (!this.proxyList[id]) {
            return false;
        }
        this.proxyList[id].totalRequest++;
    }

    errorRequest(id) {
        if (!this.proxyList[id]) {
            return false;
        }
        this.proxyList[id].totalRequest++;
        this.proxyList[id].errorRequest++;
        //if (this.proxyList.length > 100 && this.proxyList[id].totalRequest >= 8 &&
        //    (this.proxyList[id].errorRequest / this.proxyList[id].totalRequest >= 0.5)) {
        //    this.proxyList.splice(id, 1);
        //}
    }


}

module.exports = function (emitter) {
    return new ProxyHandler(emitter);
};